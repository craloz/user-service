package com.carlos.user.service.repository;

import com.carlos.user.service.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

  User findUserById(Long id);

  User findByIdNumber(Long idNumber);

  User findByEmail(String email);

}
