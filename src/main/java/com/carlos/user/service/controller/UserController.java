package com.carlos.user.service.controller;

import com.carlos.user.service.DTO.LikeDto;
import com.carlos.user.service.DTO.ResponseDto;
import com.carlos.user.service.DTO.UserDto;
import com.carlos.user.service.enums.StatusType;
import com.carlos.user.service.exception.LogicException;
import com.carlos.user.service.model.User;
import com.carlos.user.service.service.LikeService;
import com.carlos.user.service.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
@Slf4j
@CrossOrigin
public class UserController {

  @Autowired
  private UserService userService;

  @Autowired
  private LikeService likeService;

  @PostMapping
  public ResponseEntity<ResponseDto> createUser(@RequestBody UserDto userDto) {
    try {
      return new ResponseEntity<ResponseDto>(new ResponseDto("USER_CREATED", UserDto.fromEntity(userService.createUser(userDto.toEntity())), StatusType.SUCCESS), HttpStatus.OK);

    } catch (LogicException le){

      log.error(le.getMessage());
      return new ResponseEntity<ResponseDto>(new ResponseDto(le.getMessage(), StatusType.FAILURE), HttpStatus.BAD_REQUEST);

    } catch (Exception e){
      e.printStackTrace();
      log.error(e.getMessage());
      return new ResponseEntity<ResponseDto>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping
  public ResponseEntity<ResponseDto> editUser(@RequestBody UserDto userDto) {
    try {
      return new ResponseEntity<ResponseDto>(new ResponseDto("USER_EDITED", UserDto.fromEntity(userService.editUser(userDto.toEntity())), StatusType.SUCCESS), HttpStatus.OK);

    } catch (LogicException le){

      log.error(le.getMessage());
      return new ResponseEntity<ResponseDto>(new ResponseDto(le.getMessage(), StatusType.FAILURE), HttpStatus.BAD_REQUEST);

    } catch (Exception e){
      e.printStackTrace();
      log.error(e.getMessage());
      return new ResponseEntity<ResponseDto>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/user/{userId}")
  public ResponseEntity<ResponseDto> getUserById(@PathVariable() Long userId) {
    try {
      return new ResponseEntity<ResponseDto>(new ResponseDto("USER", UserDto.fromEntity(userService.getUserById(userId)), StatusType.SUCCESS), HttpStatus.OK);

    } catch (LogicException le){

      log.error(le.getMessage());
      return new ResponseEntity<ResponseDto>(new ResponseDto(le.getMessage(), StatusType.FAILURE), HttpStatus.BAD_REQUEST);

    } catch (Exception e){
      e.printStackTrace();
      log.error(e.getMessage());
      return new ResponseEntity<ResponseDto>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/list")
  public ResponseEntity<ResponseDto> getAllUsers() {
    try {
      return new ResponseEntity<ResponseDto>(new ResponseDto("LIST_USER", this.userService.getAllUser().stream().map(i-> UserDto.fromEntity(i)).collect(Collectors.toList()), StatusType.SUCCESS), HttpStatus.OK);

    } catch (Exception e){
      e.printStackTrace();
      log.error(e.getMessage());
      return new ResponseEntity<ResponseDto>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/user/{userId}/likes")
  public ResponseEntity<ResponseDto> createLike(@PathVariable Long userId, @RequestBody LikeDto likeDto) {
    try {
      return new ResponseEntity<ResponseDto>(new ResponseDto("LIKE_CREATED", this.likeService.createLike(userId, likeDto), StatusType.SUCCESS), HttpStatus.OK);

    } catch (Exception e){
      e.printStackTrace();
      log.error(e.getMessage());
      return new ResponseEntity<ResponseDto>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/user/likes")
  public ResponseEntity<ResponseDto> editLike(@RequestBody LikeDto likeDto) {
    try {
      return new ResponseEntity<ResponseDto>(new ResponseDto("LIKE_EDITED", this.likeService.editLike(likeDto), StatusType.SUCCESS), HttpStatus.OK);

    } catch (Exception e){
      e.printStackTrace();
      log.error(e.getMessage());
      return new ResponseEntity<ResponseDto>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


}
