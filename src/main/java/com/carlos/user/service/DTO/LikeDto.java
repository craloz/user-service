package com.carlos.user.service.DTO;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LikeDto {

  private Long id;

  private String name;

  private Long userId;

  private UserDto user;

}
