package com.carlos.user.service.DTO;

import com.carlos.user.service.enums.StatusType;
import lombok.Data;

@Data
public class ResponseDto<T> {

  private String code;

  private T body;

  private StatusType type;

  public ResponseDto() {
  }

  public ResponseDto(String code, T body, StatusType type) {
    this.code = code;
    this.body = body;
    this.type = type;
  }

  public ResponseDto(String code, StatusType type) {
    this.code = code;
    this.type = type;
  }
}
