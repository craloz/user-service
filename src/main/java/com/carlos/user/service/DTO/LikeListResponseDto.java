package com.carlos.user.service.DTO;

import lombok.Data;

import java.util.List;

@Data
public class LikeListResponseDto {

  List<LikeDto> body;

}
