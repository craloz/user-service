package com.carlos.user.service.DTO;

import com.carlos.user.service.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

  private Long id;

  private String firstName;

  private String lastName;

  private Long idNumber;

  private String email;

  private Long telephone;

  private List<LikeDto> likes;

  public User toEntity() {

    return new User(id, firstName, lastName, idNumber, email, telephone, null);

  }

  public static UserDto fromEntity(User user) {
    return new UserDto(user.getId(), user.getFirstName(), user.getLastName(), user.getIdNumber(), user.getEmail(), user.getTelephone(), user.getLikes());
  }

}
