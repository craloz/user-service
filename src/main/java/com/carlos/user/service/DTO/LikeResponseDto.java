package com.carlos.user.service.DTO;

import lombok.Data;

@Data
public class LikeResponseDto {

  private LikeDto body;

}
