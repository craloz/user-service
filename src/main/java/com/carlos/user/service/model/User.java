package com.carlos.user.service.model;

import com.carlos.user.service.DTO.LikeDto;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @NotNull
  private String firstName;

  private String lastName;

  @NotNull
  @Column(unique = true)
  private Long idNumber;

  @NotNull
  @Column(unique = true)
  private String email;

  private Long telephone;

  @Transient
  private List<LikeDto> likes;

}
