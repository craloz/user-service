package com.carlos.user.service.service;

import com.carlos.user.service.DTO.*;
import com.carlos.user.service.exception.LogicException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;

@Service

public class LikeService {

  @Autowired
  private RestTemplate restTemplate;

  @Autowired
  private UserService userService;

  public List<LikeDto> getLikesByUserId(Long userId) {
    return restTemplate.getForObject("http://localhost:12123/likes/" + userId, LikeListResponseDto.class).getBody();
  }

  public LikeDto createLike(Long userId, LikeDto likeDto) throws LogicException {

    likeDto.setUser(UserDto.fromEntity(userService.getUserById(userId)));
    return restTemplate.postForObject("http://localhost:12123/likes/", likeDto, LikeResponseDto.class).getBody();
  }

  public LikeDto editLike(LikeDto likeDto) throws LogicException {
    return restTemplate.exchange("http://localhost:12123/likes/", HttpMethod.PUT, new HttpEntity<LikeDto>(likeDto, null), LikeResponseDto.class, new HashMap<String, String>()).getBody().getBody();
  }

}
