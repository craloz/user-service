package com.carlos.user.service.service;
import com.carlos.user.service.DTO.LikeDto;
import com.carlos.user.service.exception.LogicException;
import com.carlos.user.service.model.User;
import com.carlos.user.service.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class UserService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private LikeService likeService;

  public User createUser(User user) throws LogicException {

    user.setId(null);

    if (user.getEmail() == null || user.getEmail().isEmpty()) {
      throw new LogicException("EMAIL_CAN_NOT_BE_VOID");
    }

    if (user.getFirstName() == null || user.getFirstName().isEmpty()) {
      throw new LogicException("FIRST_NAME_CAN_NOT_BE_VOID");
    }

    if (user.getIdNumber() == null) {
      throw new LogicException("ID_NUMBER_CAN_NOT_BE_VOID");
    }

    if (user.getTelephone() == null) {
      throw new LogicException("TELEPHONE_CAN_NOT_BE_VOID");
    }

    if (!isValidEmail(user.getEmail())) {
      throw new LogicException("INVALID_EMAIL_FORMAT");
    }

    if (getUserByEmail(user.getEmail()) != null) {
      throw new LogicException("EMAIL_ALREADY_IN_USE");
    }

    if (getUserByIdNumber(user.getIdNumber()) != null) {
      throw new LogicException("ID_NUMBER_ALREADY_IN_USE");
    }

    return userRepository.save(user);
  }

  public User editUser(User user) throws LogicException {

    if (user.getEmail() == null || user.getEmail().isEmpty()) {
      throw new LogicException("EMAIL_CAN_NOT_BE_VOID");
    }

    if (user.getFirstName() == null || user.getFirstName().isEmpty()) {
      throw new LogicException("FIRST_NAME_CAN_NOT_BE_VOID");
    }

    if (user.getIdNumber() == null) {
      throw new LogicException("ID_NUMBER_CAN_NOT_BE_VOID");
    }

    if (user.getTelephone() == null) {
      throw new LogicException("TELEPHONE_CAN_NOT_BE_VOID");
    }

    User savedUser = getUserById(user.getId());

    if (!isValidEmail(savedUser.getEmail())) {
      throw new LogicException("INVALID_EMAIL_FORMAT");
    }

    if (getUserByEmail(user.getEmail()) != null && !user.getEmail().equals(savedUser.getEmail())) {
      throw new LogicException("EMAIL_ALREADY_IN_USE");
    }

    if (getUserByIdNumber(user.getIdNumber()) != null && !user.getIdNumber().equals(savedUser.getIdNumber())) {
      throw new LogicException("ID_NUMBER_ALREADY_IN_USE");
    }

    savedUser.setFirstName(user.getFirstName());
    savedUser.setLastName(user.getLastName());
    savedUser.setIdNumber(user.getIdNumber());
    savedUser.setEmail(user.getEmail());
    savedUser.setTelephone(user.getTelephone());

    return userRepository.save(savedUser);
  }

  public User getUserById(Long id) throws LogicException {

    User user = userRepository.findUserById(id);

    if (user == null) {
      throw new LogicException("USER_NOT_FOUND");
    }

    user.setLikes(likeService.getLikesByUserId(user.getId()));

    return user;
  }

  public List<User> getAllUser() {

    List<User> users = userRepository.findAll();

    users.stream().map(user -> {
      user.setLikes(likeService.getLikesByUserId(user.getId()));
      return user;
    }).collect(Collectors.toList());

    return users;
  }

  public User getUserByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  public User getUserByIdNumber(Long idNumber) {
    return userRepository.findByIdNumber(idNumber);
  }

  public boolean isValidEmail(String email) {

    Pattern pattern = Pattern.compile("^(.+)@(.+)$");
    Matcher matcher = pattern.matcher(email);
    return matcher.matches();
  }

}
